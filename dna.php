<?php 
$title = "DNA | Akropolis Transporte";
$description = "Clique e conheça a Akropolis Transporte, especialista em transporte executivo, com motoristas particulares especializados. Confira!";
$keyword = "Missão, Valores, Visão, Transportes";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "DNA";
$bannerImg = 'img/quem-somos.jpg';
$bannerAlt = 'Akropolis Transporte';
$idliMenu = '#liDna';
include 'header.php';
include 'banner.php';
?>

<div class="container text-center texto">
  <br/>
  <p>A <span>Akropolis Transporte Executivo</span> foi criada com o objetivo de oferecer um atendimento diferenciado de alto padrão, com qualidade, segurança, confiabilidade e profissionalismo.</p>
  <p>Numa Metrópole como São Paulo, principal centro financeiro, corporativo e mercantil da América do Sul, identificamos a oportunidade em criar soluções confiáveis com o perfil personalizado que atenda às necessidades de mobilidade de nossos clientes.</p>
  <p>O nome "Akropolis" provém da Grécia, as acrópoles da antiga Grécia eram "cidades altas" (do grego ****, "alto" e ****. "pólis"), construídas no ponto mais elevado das cidades, serviam originalmente como proteção contra invasores de cidades inimigas, e nós da Akropolis Transporte Executivo através dessa definição conseguimos transparecer a nossa essência e dessa forma visamos sempre oferecer os melhores serviços para atender à sua necessidade.</p>
  <p>Com profissionais qualificados e frota de veículos que seguem as normas brasileiras de segurança, estamos devidamente preparados para oferecer o melhor serviço de transportes para a sua comodidade e segurança.</p>
  <a href="contato.php">
    <div id="txorcamento">
      <p>Solicite um <span>orçamento</span> e conheça os nossos serviços.</p>
    </div>
  </a>

</div>

<div class="clientes container text-center">    
  <h3>Nossos Clientes</h3><br>
  <div class="row">
    <div class="clientes-logos">
      <img src="img/dropbox.png" class="img-responsive" alt="Dropbox">
    </div>
    <div class="clientes-logos"> 
      <img src="img/Camara.png" class="img-responsive" alt="Camara">
    </div>
    <div class="clientes-logos">
      <img src="img/gmr.png" class="img-responsive" alt="GMR">
    </div>
    <div class="clientes-logos"> 
      <img src="img/redbull.jpg" class="img-responsive" alt="redb">
    </div>
    <div class="clientes-logos">
      <img src="img/neshoes.jpg" class="img-responsive" alt="netsh">
    </div>

  </div>
</div><br>

<div class="clientes container text-center">
  <div class="row">

    <div class="clientes-logos"> 
      <img src="img/UFI.JPG" class="img-responsive" alt="ufi">
    </div>
    <div class="clientes-logos"> 
      <img src="img/Servired.jpg" class="img-responsive" alt="Servired">
    </div>
    <div class="clientes-logos"> 
      <img src="img/abaco.jpg" class="img-responsive" alt="Abaco">
    </div>
    <div class="clientes-logos"> 
      <img src="img/Rocha.png" class="img-responsive" alt="rocha">
    </div>
    <div class="clientes-logos"> 
      <img src="img/patria.jpg" class="img-responsive" alt="patria">
    </div>
  </div>
</div><br>
<?php include 'footer.php' ?>