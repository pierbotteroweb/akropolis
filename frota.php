<?php 
$title = "Veículos de Luxo - SUVS e Sedans | Akropolis Transporte";
$description = "Confira todos os carros de luxo que integram a nossa frota, com bancos de couro, ar condicionado e até modelos blindados. Aproveite!";
$keyword = "Suv, carro executivo, carro importado, sedan, sedan de luxo, carro blindado";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "FROTA";
$bannerImg = 'img/frota.jpg';
$bannerAlt = 'Akropolis Transporte Frota';
$idliMenu = '#liFleet';
include 'header.php';
include 'banner.php';
?>

<div class="container text-center texto">
  <br/>
  <p>Nós da <span>Akropolis Transporte Executivo</span> trabalhamos com veículos nacionais e importados com seguro total e de passageiros, equipados com itens de segurança, conforto e tecnologia.</p>
  <p>Possuímos veículos executivos com bancos de couro, ar condicionado e airbags e estão preparados para atender viagens de curta, média e longa distância, executando um trajeto de maneira ágil, segura e confortável, garantindo o melhor custo benefício para os nossos clientes.</p>
  <p>Solicite um orçamento e receba a melhor alternativa de veiculo para o seu destino de acordo com sua necessidade.</p>
  <p id="nota">*Para veículos blindados consulte disponibilidade</p>
  <a href="contato.php">
    <div id="txorcamento">
     <p>SOLICITE UM ORÇAMENTO.</p>
    </div>
  </a>
</div>

<div class="container text-center">
  <div class="row">

    <div class="col-md-6 veiculos"> 
      <img src="img/cerato.jpg" class="img-responsive" alt="cerato">
    </div>
    <div class="col-md-6 veiculos"> 
      <img src="img/corolla.jpg" class="img-responsive" alt="corolla">
    </div>
  </div>
</div><br>
<?php include 'footer.php' ?>