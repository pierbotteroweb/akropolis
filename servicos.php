<?php 
$title = "Transporte Executivo para Hoteis e Aeroportos | Akropolis Transporte";
$description = "Conheça nossos serviços de transporte executivos para hotéis, translados, viagens, contratações diárias e para aeroportos. Confira!";
$keyword = "Transporte para Hotéis, Transporte para Aeroportos, Transporte executivo, translados";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "SERVIÇOS";
$bannerImg = 'img/servicos.jpg';
$bannerAlt = 'Akropolis Transporte Serviços';
$idliMenu = '#liServices';
include 'header.php';
include 'banner.php';
?>

<div class="container text-center texto">
  <br/>
  <p>A <span>Akropolis Transporte Executivo</span> dispõe de diversos serviços e alternativas de acordo com a conveniência de cada um. Confira a seguir e solicite seu orçamento:</p>
</div>
<div class="desc-servicos container text-center">
  <div class="row">
    <div class="desc">
                  <h2>Transporte <span>para Hoteis</span></h2><br/>
                  <p>Para hotéis, shows, eventos, convenções, reuniões corporativas, restaurantes, aeroportos e outros locais.</p>
    </div>
    <div class="desc">
                  <h2>Aeroportos <span>para Aeroportos</span></h2><br/>
                  <p>Realizamos receptivos nos principais aeroportos de São Paulo, Grande São Paulo e interior.</p>
    </div>
    <div class="desc">
                  <h2>Viagens</h2><br/>
                  <p>Interestaduais e Intermunicipais. Oferecemos transporte para viagens de negócios, lazer e outros.</p>
    </div>
    <div class="desc">
                  <h2>Diárias <span>de Motoristas Particulares</span></h2><br/>
                  <p>Fornecemos veículos com  motorista a disposição do cliente.<br/><br/></p>
    </div>
  </div>
</div>
<?php include 'footer.php' ?>