<?php 
$title = "Região de Atendimento | Akropolis Transporte";
$description = "Confira todos os carros de luxo que integram a nossa frota, com bancos de couro, ar condicionado e até modelos blindados. Aproveite!";
$keyword = "Suv, carro executivo, carro importado, sedan, sedan de luxo, carro blindado";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "REGIÃO DE ATENDIMENTO";
$bannerImg = 'img/regiao.jpg';
$bannerAlt = 'Akropolis Transporte Região de Atendimento';
$idliMenu = '#liArea';
include 'header.php';
include 'banner.php';
?>

<div class="texto container text-center">
  <br/>
  <p>Estamos localizados na cidade de São Paulo, e atendemos capital, Grande São Paulo, interior de São Paulo e fazemos trajetos interestaduais, e intermunicipais.</p>
  <p>Entre em contato para conhecer os nossos serviços e solicite um orçamento.</p>
</div>

<div class="container">
    <div class="mapa texto"> 
       <img src="img/mapa.jpg" class="img-responsive" alt="mapa">
       <a href="contato.php">
          <div id="txorcamento">
              <p>SOLICITE UM ORÇAMENTO.</p>
          </div>
       </a>
    </div>
</div>
<br>
<?php include 'footer.php' ?>