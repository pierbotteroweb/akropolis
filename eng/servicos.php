<?php 
$title = "Transporte Executivo para Hoteis e Aeroportos | Akropolis Transporte";
$description = "Conheça nossos serviços de transporte executivos para hotéis, translados, viagens, contratações diárias e para aeroportos. Confira!";
$keyword = "Transporte para Hotéis, Transporte para Aeroportos, Transporte executivo, translados";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "SERVICES";
$bannerImg = '../img/servicos.jpg';
$bannerAlt = 'Akropolis Transporte Serviços';
$idliMenu = '#liServices';
include 'header.php';
include 'banner.php';
?>

<div class="container text-center texto">
  <br/>
  <p>A <span>Akropolis Executive Transportation</span> has several services and alternatives according to each one's convenience. Check below and request a quote:</p>
</div>
<div class="desc-servicos container text-center">
  <div class="row">
    <div class="desc">
                  <h2>Transfer <span>for hotels</span></h2><br/>
                  <p>Hotels, shows, events, business meetings, restaurants, airports and other locations.</p>
    </div>
    <div class="desc">
                  <h2>Airports <span>to Airports</span></h2><br/>
                  <p>We offer pick-up service for main São Paulo City Area airports and country area.</p>
    </div>
    <div class="desc">
                  <h2>Trips</h2><br/>
                  <p>Interstate and County by County Transportation for business, leisure and others.</p>
    </div>
    <div class="desc">
                  <h2>Full-day <span>Private Drivers</span></h2><br/>
                  <p>We offer vehicles with private driver at your disposal.<br/><br/></p>
    </div>
  </div>
</div>
<?php include 'footer.php' ?>