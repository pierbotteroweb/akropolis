<?php 
$title = "Executive Transportation - Private Chauffeur | Akropolis Transportation";
$description = "Clique e conheça a Akropolis Transporte, especialista em transporte executivo, com motoristas particulares especializados. Confira!";
$keyword = "motorista, motorista particular, motorista particular sp";
$child = "";
$canonical = "<link rel=”canonical” href=”www.akropolistransporte.com.br/”/>";
$bing = '<meta name="msvalidate.01" content="A347B253B42283B032236FC4F6C962B4" />';
$analytics = '<script async src="..\js\analytics.js"></script>';
$akro = '<script async src="..\js\akro.js"></script>';
$formatDetection = '';
$idliMenu = '#liHome';
include 'header.php';
?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="..\img\rapidez.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Agility</h3>
        </div>      
      </div>

      <div class="item">
        <img src="..\img\conforto.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Comfort</h3>
        </div>      
      </div>

      <div class="item">
        <img src="..\img\destinos.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Destinations</h3>
        </div>  
          
      </div>

      <div class="item">
        <img src="..\img\seguranca.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Safety</h3>
        </div> 
      </div>
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>

</div>
  
<div class="container text-center" id="txt-principal">    
  <h1>AKROPOLIS EXECUTIVE TRANSPORTATION</h1><br>
  <p>Here you find all needed information regarding our coverage and services. We are at your disposal offering an outstanding, high standard service with the quality, safety, reliability and professionalism that you look for. Meet our services and vehicles, Akropolis Executive Transportation is your best choice.</p>
</div><br>

<div class="container text-center thumbs">    
  <div class="row">
    <div class="thumbs-col col-md-15">
        <p>Our Fleet</p>
        <a href="frota.php"><img class="tb" src="..\img\thumb1.jpg" alt="Akropolis Frota"></a><br><br>
        <p>Meet our high standard vehice fleet.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Services</p>
        <a href="servicos.php"><img class="tb" src="..\img\thumb2.jpg" alt="Akropolis Serviços"></a><br><br>
        <p>We offer alternative options according to your convenience.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Service Area</p>
        <a href="regiao.php"><img class="tb" src="..\img\thumb3.jpg" alt="Akropolis Atendimento"></a><br><br>
        <p>We are located at São Paulo State capital. Learn more about our service area.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Quote</p>
        <a href="contato.php"><img class="tb" src="..\img\thumb4.jpg" alt="Akropolis Orçamento"></a><br><br>
        <p>Request a quote and meet our services.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Contact</p>
        <a href="contato.php"><img class="tb" src="..\img\thumb5.jpg" alt="Akropolis Contato"></a><br><br>
        <p>Get in touch with Akropolis Executive Transportation and learn more about us.</p>
    </div>

  </div>
</div><br>
<?php include 'footer.php' ?>