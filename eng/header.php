<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo $title; ?></title>
  <meta charset="utf-8">
  <meta name="author" content="Pier Bottero Web"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php echo $description; ?>"/>
  <meta name="keywords" content=" <?php echo $keyword; ?>">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" href="..\img\favicon.ico" type="image/x-icon" />
  <link href='https://fonts.googleapis.com/css?family=Secular+One' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet">
  <link rel="stylesheet" href="..\css\style.css">
  <?php echo $bing; ?>
  <?php echo $analytics;?>
  <?php echo $formatDetection; ?>
  <style type="text/css">
    <?php echo $idliMenu ?>{
    background-color: #333333 !important; 
  }
  </style>

</head>

<body>

<nav class="navbar navbar-default">
  <div class="navbar-header-wrap container-fluid">
    <div class="navbar-header">
      <a id="logo" href="#"><img src="..\img\ak-logo.jpg"></a>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" id="textobranco">
        <li id="liHome"><a href="index.php">HOME</a></li>
        <li id="liDna"><a href="dna.php">DNA</a></li>
        <li id="liServices"><a href="servicos.php">SERVICES</a></li>
        <li id="liFleet"><a href="frota.php">FLEET</a></li>
        <li id="liArea"><a href="regiao.php">SERVICE AREA</a></li>
        <li id="liContact"><a href="contato.php">CONTACT</a></li>
        <li><a href="..\index.php" style="padding: 10px 15px"><img src="..\img\logobr.png"></a></li>
      </ul>
  </div>
  </div>
</nav>