<?php 
$title = "Veículos de Luxo - SUVS e Sedans | Akropolis Transporte";
$description = "Confira todos os carros de luxo que integram a nossa frota, com bancos de couro, ar condicionado e até modelos blindados. Aproveite!";
$keyword = "Suv, carro executivo, carro importado, sedan, sedan de luxo, carro blindado";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "FLEET";
$bannerImg = '../img/frota.jpg';
$bannerAlt = 'Akropolis Transporte Frota';
$idliMenu = '#liFleet';
include 'header.php';
include 'banner.php';
?>

<div class="container text-center texto">
  <br/>
  <p><span>Akropolis executive Transportation</span> works with national and imported vehicle, with full passenger insurance, equiped with safety optionals, comfort and technology.</p>
  <p>Our executive vehicles are available with leather seats, air conditioner, airbags, ready to attend your short, medium, or long distance trip, using the best routes in a safe, comfortable, and agile way, assuring the best cost-benefit for our clients.</p>
  <p>Request a quote and get the best vehicle option for your destination according to your need.</p>
  <p id="nota">*For armored vehicles, please consult us about availability.</p>
  <a href="contato.php">
    <div id="txorcamento">
     <p>REQUEST A QUOTE</p>
    </div>
  </a>
</div>

<div class="container text-center">
  <div class="row">

    <div class="col-md-6 veiculos"> 
      <img src="../img/cerato.jpg" class="img-responsive" alt="cerato">
    </div>
    <div class="col-md-6 veiculos"> 
      <img src="../img/corolla.jpg" class="img-responsive" alt="corolla">
    </div>
  </div>
</div><br>
<?php include 'footer.php' ?>