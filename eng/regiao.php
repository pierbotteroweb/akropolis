<?php 
$title = "Região de Atendimento | Akropolis Transporte";
$description = "Confira todos os carros de luxo que integram a nossa frota, com bancos de couro, ar condicionado e até modelos blindados. Aproveite!";
$keyword = "Suv, carro executivo, carro importado, sedan, sedan de luxo, carro blindado";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "SERVICE AREA";
$bannerImg = '../img/regiao.jpg';
$bannerAlt = 'Akropolis Transporte Região de Atendimento';
$idliMenu = '#liArea';
include 'header.php';
include 'banner.php';
?>

<div class="texto container text-center">
  <br/>
  <p>We are located at São Paulo City, attending the state capital, metropolitan area, county area by using interstate and county by county routes.</p>
  <p>Contact us and get a quote for more information about our services.</p>
</div>

<div class="container">
    <div class="mapa texto"> 
       <img src="../img/mapa.jpg" class="img-responsive" alt="mapa">
       <a href="contato.php">
          <div id="txorcamento">
              <p>GET A QUOTE</p>
          </div>
       </a>
    </div>
</div>
<br>
<?php include 'footer.php' ?>