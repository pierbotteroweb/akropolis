<?php 
$title = "DNA | Akropolis Transporte";
$description = "Clique e conheça a Akropolis Transporte, especialista em transporte executivo, com motoristas particulares especializados. Confira!";
$keyword = "Missão, Valores, Visão, Transportes";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$akro = '';
$formatDetection = '';
$bannerH1 = "DNA";
$bannerImg = '../img/quem-somos.jpg';
$bannerAlt = 'Akropolis Transporte';
$idliMenu = '#liDna';
include 'header.php';
include 'banner.php';
?>

<div class="container text-center texto">
  <br/>
  <p><span>Akropolis Executive Transportation</span> was created with its main goal as to offer a high standard service with quality, safety, reliability and professionalism.</p>
  <p>In a metropolis as São Paulo, the main financial, corporative and mercantile capital of South America, we see the opportunity for the development of reliable solutions with a personalized profile that meets our clients mobility needs.</p>
  <p>The name "Akropolis" is originated from Greece. The Old Greece acropolis were "high cities" (ἄκρος, "high, extreme", and πόλις, "city") built at the city's highest level with it's primary purpose of protection against the invasion of enemy cities. And we Akropolis Executive Transportation, by this definition can transmit our essence and thereby always seek to fit your need by offering the best service.</p>
  <p>With qualified professionals and a vehicle fleet that meet brazilian safety standards, we are properly ready to offer the best transportation service for your convenience and safety.</p>
  <a href="contato.php">
    <div id="txorcamento">
      <p>Request a <span>quote</span> and learn about our services.</p>
    </div>
  </a>

</div>

<div class="clientes container text-center">    
  <h3>Our Clients</h3><br>
  <div class="row">
    <div class="clientes-logos">
      <img src="../img/dropbox.png" class="img-responsive" alt="Dropbox">
    </div>
    <div class="clientes-logos"> 
      <img src="../img/Camara.png" class="img-responsive" alt="Camara">
    </div>
    <div class="clientes-logos">
      <img src="../img/gmr.png" class="img-responsive" alt="GMR">
    </div>
    <div class="clientes-logos"> 
      <img src="../img/redbull.jpg" class="img-responsive" alt="redb">
    </div>
    <div class="clientes-logos">
      <img src="../img/neshoes.jpg" class="img-responsive" alt="netsh">
    </div>

  </div>
</div><br>

<div class="clientes container text-center">
  <div class="row">

    <div class="clientes-logos"> 
      <img src="../img/UFI.JPG" class="img-responsive" alt="ufi">
    </div>
    <div class="clientes-logos"> 
      <img src="../img/Servired.jpg" class="img-responsive" alt="Servired">
    </div>
    <div class="clientes-logos"> 
      <img src="../img/abaco.jpg" class="img-responsive" alt="Abaco">
    </div>
    <div class="clientes-logos"> 
      <img src="../img/Rocha.png" class="img-responsive" alt="rocha">
    </div>
    <div class="clientes-logos"> 
      <img src="../img/patria.jpg" class="img-responsive" alt="patria">
    </div>
  </div>
</div><br>
<?php include 'footer.php' ?>