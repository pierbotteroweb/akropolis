<?php 
$title = "Transporte Executivo - Motoristas Particulares | Akropolis Transporte";
$description = "Clique e conheça a Akropolis Transporte, especialista em transporte executivo, com motoristas particulares especializados. Confira!";
$keyword = "motorista, motorista particular, motorista particular sp";
$child = "";
$canonical = "<link rel=”canonical” href=”www.akropolistransporte.com.br/”/>";
$bing = '<meta name="msvalidate.01" content="A347B253B42283B032236FC4F6C962B4" />';
$analytics = '<script async src="js/analytics.js"></script>';
$akro = '<script async src="js/akro.js"></script>';
$formatDetection = '';
$idliMenu = '#liHome';
include 'header.php';
?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="img\rapidez.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Rapidez</h3>
        </div>      
      </div>

      <div class="item">
        <img src="img\conforto.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Conforto</h3>
        </div>      
      </div>

      <div class="item">
        <img src="img\destinos.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Destinos</h3>
        </div>  
          
      </div>

      <div class="item">
        <img src="img\seguranca.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>Segurança</h3>
        </div> 
      </div>
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>

</div>
  
<div class="container text-center" id="txt-principal">    
  <h1>AKROPOLIS TRANSPORTE EXECUTIVO</h1><br>
  <p>Aqui você encontrará todas as informações necessárias sobre nossa atuação e serviços. Estamos à sua disposição para oferecer um atendimento diferenciado com alto padrão de qualidade, segurança, confiabilidade e profissionalismo que você procura. Conheça os nossos serviços e transportes, a Akropolis Transporte Executivo é a sua melhor escolha.</p>
</div><br>

<div class="container text-center thumbs">    
  <div class="row">
    <div class="thumbs-col col-md-15">
        <p>Nossa Frota</p>
        <a href="frota.php"><img class="tb" src="img\thumb1.jpg" alt="Akropolis Frota"></a><br><br>
        <p>Conheça a nossa frota com transporte de alto padrão.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Serviços</p>
        <a href="servicos.php"><img class="tb" src="img\thumb2.jpg" alt="Akropolis Serviços"></a><br><br>
        <p>Oferecemos diversas alternativas de acordo com a sua conveniência.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Região de Atendimento</p>
        <a href="regiao.php"><img class="tb" src="img\thumb3.jpg" alt="Akropolis Atendimento"></a><br><br>
        <p>Estamos localizados na capital de são Paulo. Saiba mais sobre as regiões de atendimento.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Orçamento</p>
        <a href="contato.php"><img class="tb" src="img\thumb4.jpg" alt="Akropolis Orçamento"></a><br><br>
        <p>Solicite um orçamento e conheça os nossos serviços.</p>
    </div>
    <div class="thumbs-col col-md-15">
        <p>Contato</p>
        <a href="contato.php"><img class="tb" src="img\thumb5.jpg" alt="Akropolis Contato"></a><br><br>
        <p>Entre em contato com a Akropolis Transporte Executivo e saiba mais.</p>
    </div>

  </div>
</div><br>
<?php include 'footer.php' ?>