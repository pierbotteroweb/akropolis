

<footer class="container-fluid text-center">   
  <div class="row">
  <div class="col-sm-2">
        <p>INSTITUCIONAL</p>
        <a href="dna.php"><p style="color:#fff;font-weight:normal;">DNA</p></a>
        <a href="servicos.php"><p style="color:#fff;font-weight:normal;">Serviços</p></a>
        <a href="frota.php"><p style="color:#fff;font-weight:normal;">Frota</p></a>
  </div>
  <div class="col-sm-2">
        <p>ATENDIMENTO</p>
        <a href="regiao.php"><p style="color:#fff;font-weight:normal;">Região de Atendimento</p></a>
        <a href="contato.php"><p style="color:#fff;font-weight:normal;">Contato</p></a>
  </div>
  <div class="col-sm-2">
        <p>REDES SOCIAIS</p>
        <a href="https://www.facebook.com/akropolistransporte" target="_blank"><p style="color:#fff;font-weight:normal;">Facebook</p></a>
        <a href="https://www.linkedin.com/company/akropolis-transporte" target="_blank"><p style="color:#fff;font-weight:normal;">Linkedin</p></a>
  </div>


  <div class="col-sm-2 contato-footer">     
      <p>CONTATO</p>
      <p><img src="img\contel.png" class="img-footer-small" alt="Telefone Akropolis Transporte">
      Telefone Fixo</p>
      <p>+55 (11) 2305-5153<p/>
      <p><img src="img\conmail.png" class="img-footer-small alt="Email Akropolis Transporte">
      E-mail</p><p>contato@akropolistransporte.com.br<p/>
      <p><img src="img\conwhats.png" class="img-footer-small" alt="Whatsapp Akropolis Transporte">
      WhatsApp</p><p>+55 (11) 99536-6680 ou<br>+55 (11) 94276-3888<p/>
  </div>


  <div class="col-sm-2"></div>
  <div class="col-sm-2">
    <div class="logos-redes-sociais">
      <a href="index.php"><img src="img\aklogo.png" alt="Akropolis Transporte"></a>
      <a href="https://www.facebook.com/akropolistransporte" target="_blank"><img src="img\facebook.png" alt="Facebook"></a>
      <a href="https://www.linkedin.com/company/akropolis-transporte" target="_blank"><img src="img\linkedin.png" alt="Linkedin"></a><br>
      <span>Siga-nos nas redes sociais</span>
    </div>
  </div>

  </div>
</footer>


</body>

  <?php echo $akro; ?>

</html>

