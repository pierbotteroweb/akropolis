<?php 
$title = "Fale Conosco | Akropolis Transporte";
$description = "Entre em contato com a Akropolis Transporte e esclareça todas as suas duvidas sobre nossos serviços, frotas e valores!";
$keyword = "Contato, Fale conosco";
$child = "";
$canonical = "";
$bing = '';
$analytics = '';
$formatDetection = '<meta name="format-detection" content="telephone=no">';
$akro = '';
$bannerH1 = "CONTATO";
$bannerImg = 'img/contato.jpg';
$bannerAlt = 'Akropolis Transporte Contato';
$idliMenu = '#liContact';
include 'header.php';
include 'banner.php';
?>

<form id="formulario" method="post" action="http://akropolistransporte.com.br/cgi-sys/formmail.pl"/> 
  <input type="hidden" name="recipient" value="contato@akropolistransporte.com.br"/>
  
  <input type="hidden" name="subject" value="Akropolis Transporte - Contato"/><input type="hidden" name="redirect" value="http://akropolistransporte.com.br/sucesso.html"/>

  <table width="50%" style="margin:auto;"> 

    <tr>
    <td height="19" width="100%"><input type="text" name="nome" size="40" placeholder="NOME" required></td>
    <td height="19" width="100%"><input type="text" name="empresa" size="40" placeholder="EMPRESA" required></td>
    </tr> 

    <tr>
    <td height="19" width="100%"><input type="text" name="email" size="40" placeholder="E-MAIL" required></td> 
    <td height="19" width="100%"><input type="text" name="telefone" size="40" placeholder="TELEFONE" required></td> 
    </tr> 

    <tr>
    <td height="19" width="100%" colspan="2"><textarea name="mensagem" placeholder="MENSAGEM" required></textarea></td>
    </tr> 

    <tr>
    <td height="19" width="100%">
    <input type="submit" name="Submit" value="Enviar">
    </td>
    </tr> 

  </table>
</form>
<div class="contato container text-center">
  <div class="row">
    <div class="coluna-contato col-sm-4">
      <img src="img\contel.jpg" class="img-responsive" alt="Telefone Akropolis Transporte">
      <p>Telefone Fixo</p>
<p>+55 (11) 2305-5153<p/>
    </div>
    <div class="coluna-contato col-sm-4">
      <img src="img\conmail.jpg" class="img-responsive" alt="Email Akropolis Transporte">
      <p>E-mail</p><p>contato@akropolistransporte.com.br<p/>
    </div>
    <div class="coluna-contato col-sm-4">
      <img src="img\conwhats.jpg" class="img-responsive" alt="Whatsapp Akropolis Transporte">
      <p>WhatsApp</p><p>+55 (11) 99536-6680 ou +55 (11) 94276-3888<p/>
    </div>
  </div>
</div><br>
<?php include 'footer.php' ?>